package br.com.erasmo.icaro.parser;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class Main {

	private static Map<String, String> vars = new HashMap<>();
	private static Map<String, String> xpaths = new HashMap<>();
	private static Map<String, String> expressions = new HashMap<>();
	
	public static void main(String[] args) {
		vars.put("nooooooome", "Icaro");
		vars.put("Icaro", "mizerê");
		xpaths.put("part1", "nooooooo");
		xpaths.put("part2", "me");
		expressions.put("varAnnotation", "#");
		
		String text = "#{!{varAnnotation}{@{part1}@{part2}}}";
		System.out.println(parse(text));
	}
	
	private static String parse(String text) {
		return parse(0, '{', '}', text);
	}

	private static String parse(Integer n, char openChar, char closeChar, String text) {
		
		StringBuilder strBuilder = new StringBuilder(text);
		
		Stack<Integer> stack = new Stack<>();
		
		for(int i = n; i < strBuilder.length(); i++) {
			String chaR = ""+strBuilder.charAt(i);
			if(chaR.equals(""+openChar)) {
				stack.push(i);
			}
			if(chaR.equals(""+closeChar)) {
				int start = stack.pop();
				replace(strBuilder, start, i);
				i = start - 1;
			}
		}
		
		return strBuilder.toString();
	}

	public static void replace(StringBuilder strBuilder, int start, int i) {
		
		String value = null;
		
		switch(strBuilder.charAt(start-1)) {
		case '@':
			value = xpaths.get(strBuilder.substring(start+1, i));
			strBuilder.replace(start-1, i+1, value != null ? value : "");
			break;
		case '#':
			value = vars.get(strBuilder.substring(start+1, i));
			strBuilder.replace(start-1, i+1, value != null ? value : "");
			break;
		case '!':
			value = expressions.get(strBuilder.substring(start+1, i));
			strBuilder.replace(start-1, i+1, value != null ? value : "");
			break;
		default:
			break;
		}
	}
}
